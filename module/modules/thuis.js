const axios = require('axios');
const config = require('../../config')

const moment = require('moment');
moment.locale('nl');  

//https://api.9292.nl/0.1/locations?lang=nl-NL&latlong=52.492439,4.797257

//locations[0].id 

//https://api.9292.nl//0.1/journeys?before=1&sequence=1&byFerry=true&bySubway=true&byBus=true&byTram=true&byTrain=true&lang=nl-NL&from=assendelft/ijkmeester-3&dateTime=2018-04-16T1200&searchType=departure&interchangeTime=standard&after=5&to=purmerend/tangramstraat-5



module.exports = {
  setthuis: function (bot, msg, match) {
    try{
      var id = msg.from.id;
      console.log()
      message = msg.text.toLowerCase();
      message = message.split(" ");
      if(message.length == "4"){
        var address = {
          "user": ""+id+"",
          "place": ""+message[1]+"",
          "street": ""+message[2]+"",
          "number": ""+message[3]+""
        }
  
        axios.post(config.BASEURL+':'+config.PORT+'/api/Thuis', address)
          .then(response => {
            bot.sendMessage(msg.chat.id,"/setthuis is succesvol ingesteld!")
          })
          .catch(err => {
            console.log(err);
          });
      } else{
        bot.sendMessage(msg.chat.id,"Gebruik /setthuis PLAATS STRAAT NUMMER")
      }
    } catch(e){
      bot.sendMessage(msg.chat.id,"Something went wrong! Please try again.")
    }
  },

  thuis: function(bot, msg, match){
    var option = {
      "parse_mode": "Markdown",
      "reply_markup": {
          "one_time_keyboard": true,
          "keyboard": [[{
              text: "Mijn locatie",
              request_location: true
          }], ["Cancel"]]
      }
    };
    bot.sendMessage(msg.chat.id, "Hoe kunnen we je vinden?", option).then(()=>{
      bot.once("location",(msg)=>{
        var location = "";

        axios.get(config.BASEURL+':'+config.PORT+'/api/Thuis?filter={"where":{"user":'+msg.from.id+'}}')
          .then(response => {
            location = response.data[0].place + "/" + response.data[0].street + "-" + response.data[0].number

            axios.get('https://api.9292.nl/0.1/locations?lang=nl-NL&latlong='+msg.location.latitude+','+msg.location.longitude+'')
              .then(response => {
                var currentTime = moment().format("YYYY-MM-DD[T]HHmm");
                var from = response.data.locations[0].id

                axios.get('https://api.9292.nl//0.1/journeys?before=1&sequence=1&byFerry=true&bySubway=true&byBus=true&byTram=true&byTrain=true&lang=nl-NL&from='+from+'&dateTime='+currentTime+'&searchType=departure&interchangeTime=standard&after=5&to='+location)
                  .then(response => {
                    var departs = [];

                    for (let index = 0; index < response.data.journeys.length; index++) {
                      departs.push([
                        {text: "Vertrek:"+moment(response.data.journeys[index].departure).format('LT')+" -> Aankomst:"+moment(response.data.journeys[index].arrival).format('LT') ,
                        callback_data:'{"t":"Thuis","i":'+index+',"f":"'+from+'"}'}
                      ])
                    }

                    var keyboardStr = JSON.stringify({
                      inline_keyboard: departs
                    });
                    var keyboard = JSON.parse(keyboardStr);
                    bot.sendMessage(msg.chat.id, "Kies een tijd:", {reply_markup: keyboard})


                    //journey = response.data.journeys[0]
                      //deze if nog fixen
                      //if(date("d-m-y H:i", strtotime(valueB["departure"])) >= date("d-m-y H:i")) {
                        //if(moment(valueB["departure"]).diff(moment(), 'minutes') >= "5"){




                    // info = "Vertrektijd: "+moment(journey["departure"]).format('LLL')+"\n"+ 
                    //   "Aankomsttijd: "+moment(journey["arrival"]).format('LLL')+"\n--------\n";

                    // journey["legs"].forEach(function(value){
                    //   let service = "";
                    //   if(value["service"] !== undefined){service = value["service"]}
                    //   info += "Naam: "+ value["mode"]["name"] + " " + service + "\n";

                    //   if(value["mode"]["type"] == "bus" || value["mode"]["type"] == "subway" || value["mode"]["type"] == "train") {
                    //     info+="Vertrektijd: "+value["stops"][0]["departure"]+"\n";
                    //     info+="Aankomsttijd: "+value["stops"][(value["stops"].length -1)]["arrival"]+"\n";

                    //     if(value["mode"]["type"] == "train") {
                    //       info+="Perron: "+value["stops"][0]["platform"]+"\n";
                    //     }

                    //     info+="Richting: "+value["destination"]+"\n";
                    //     info+="Bestemming: "+value["stops"][(value["stops"].length -1)]["location"]["name"]+"\n";
                    //     info+="--------\n";
                    //   }

                    //   if(value["mode"]["type"] == "walk") {
                    //     info+="Bestemming: "+value["stops"][(value["stops"].length -1)]["location"]["name"]+"\n";
                    //     //deze if nog fixen
                    //     if("2" == (response.data["journeys"][0]["legs"].length -1)) {
                    //       info+="Type: OSSO\n";
                    //     } else {
                    //       if(value["stops"][(value["stops"].length -1)]["location"]["stopType"] !== undefined) {
                    //         info+="Type: "+value["stops"][(value["stops"].length -1)]["location"]["stopType"]+"\n";
                    //       }
                    //       if(value["stops"][(value["stops"].length -1)]["location"]["stationType"] !== undefined) {
                    //         info+="Type: "+value["stops"][(value["stops"].length -1)]["location"]["stationType"]+"\n";
                    //       }
                    //     }
                    //     info+="Duur: "+value["duration"]+"\n";
                    //     info+="--------\n";
                    //   }
                    // })
                    // info += "Vertrektijd: "+moment(journey["departure"]).format('LLL')+"\n"+ 
                    //   "Aankomsttijd: "+moment(journey["arrival"]).format('LLL')+"\n--------\n";
                    //   //}
                    // bot.sendMessage(msg.chat.id, info)
                  })
                  .catch(err => {
                    console.log(err);
                  });
              })
              .catch(err => {
                console.log(err);
              });
          })
          .catch(err => {
            bot.sendMessage(msg.from.id, "Voordat je /thuis kan gebruiken moet je eerst je thuis adres invullen. Gebruik /setthuis PLAATS STRAAT NUMMER")
            console.log(err);
          });
      })
    })
  },

  callback: function(bot, msg, index, from){

    var location = "";
    axios.get(config.BASEURL+':'+config.PORT+'/api/Thuis?filter={"where":{"user":'+msg.from.id+'}}')
    .then(response => {
      location = response.data[0].place + "/" + response.data[0].street + "-" + response.data[0].number

      var currentTime = moment().format("YYYY-MM-DD[T]HHmm");

      axios.get('https://api.9292.nl//0.1/journeys?before=1&sequence=1&byFerry=true&bySubway=true&byBus=true&byTram=true&byTrain=true&lang=nl-NL&from='+from+'&dateTime='+currentTime+'&searchType=departure&interchangeTime=standard&after=5&to='+location)
      .then(response => {

        var journey = response.data.journeys[index]
        var info="";

        info = "Vertrektijd: "+moment(journey["departure"]).format('LLL')+"\n"+
          "Aankomsttijd: "+moment(journey["arrival"]).format('LLL')+"\n--------\n";

        journey["legs"].forEach(function(value){
          let service = "";
          if(value["service"] !== undefined){service = value["service"]}
          info += "Naam: "+ value["mode"]["name"] + " " + service + "\n";

          if(value["mode"]["type"] == "bus" || value["mode"]["type"] == "subway" || value["mode"]["type"] == "train") {
            info+="Vertrektijd: "+value["stops"][0]["departure"]+"\n";
            info+="Aankomsttijd: "+value["stops"][(value["stops"].length -1)]["arrival"]+"\n";

            if(value["mode"]["type"] == "train") {
              info+="Perron: "+value["stops"][0]["platform"]+"\n";
            }

            info+="Richting: "+value["destination"]+"\n";
            info+="Bestemming: "+value["stops"][(value["stops"].length -1)]["location"]["name"]+"\n";
            info+="--------\n";
          }

          if(value["mode"]["type"] == "walk") {
            info+="Bestemming: "+value["stops"][(value["stops"].length -1)]["location"]["name"]+"\n";
            //deze if nog fixen
            if("2" == (response.data["journeys"][0]["legs"].length -1)) {
              info+="Type: OSSO\n";
            } else {
              if(value["stops"][(value["stops"].length -1)]["location"]["stopType"] !== undefined) {
                info+="Type: "+value["stops"][(value["stops"].length -1)]["location"]["stopType"]+"\n";
              }
              if(value["stops"][(value["stops"].length -1)]["location"]["stationType"] !== undefined) {
                info+="Type: "+value["stops"][(value["stops"].length -1)]["location"]["stationType"]+"\n";
              }
            }
            info+="Duur: "+value["duration"]+"\n";
            info+="--------\n";
          }
        })
        info += "Vertrektijd: "+moment(journey["departure"]).format('LLL')+"\n"+
          "Aankomsttijd: "+moment(journey["arrival"]).format('LLL')+"\n--------\n";

      // bot.editMessageCaption({caption:"",reply_markup:[]},{chat_id:msg.message.chat.id, message_id:msg.message.message_id});
        bot.sendMessage(msg.message.chat.id, info)

      })
      .catch(err => {
        console.log(err);
      });
    })
    .catch(err => {
      bot.sendMessage(msg.from.id, "Voordat je /thuis kan gebruiken moet je eerst je thuis adres invullen. Gebruik /setthuis PLAATS STRAAT NUMMER")
      console.log(err);
    });
  }
}