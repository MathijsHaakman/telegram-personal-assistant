const axios = require('axios');
const config = require('../../config')

module.exports = {
  action: function (bot, msg, type, match) {
    var chatId = msg.chat.id
    axios.get(config.BASEURL+':'+config.PORT+'/api/Image/url/'+type+'/'+chatId)
      .then((response) => {
        var keyboardStr = JSON.stringify({
          inline_keyboard: [
            [
            {text:'1\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":1, "image":'+response.data.image_id+'}'},
            {text:'2\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":2, "image":'+response.data.image_id+'}'},
            {text:'3\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":3, "image":'+response.data.image_id+'}'},
            {text:'4\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":4, "image":'+response.data.image_id+'}'},
            {text:'5\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":5, "image":'+response.data.image_id+'}'}
            ]
          ]
        });
        var keyboard = JSON.parse(keyboardStr);
          if(response.data.type == "image"){
            if(response.data.state == "new"){
              if(response.data.image_mime_type == "image/jpeg" || response.data.image_mime_type == "image/png"){
                bot.sendPhoto(msg.chat.id, response.data.image_url, {caption:'Reet hieronder:', reply_markup: keyboard})
              } else if(response.data.image_mime_type == "image/gif"){
                bot.sendVideo(msg.chat.id, response.data.image_url, {caption:'Reet hieronder:', reply_markup: keyboard})
              }
          } else{
              if(response.data.image_mime_type == "image/jpeg" || response.data.image_mime_type == "image/png"){
                bot.sendPhoto(msg.chat.id, response.data.image_url)
              } else if(response.data.image_mime_type == "image/gif"){
                bot.sendVideo(msg.chat.id, response.data.image_url)
              }
            }
          } else if(response.data.type == "video"){
            bot.sendVideo(msg.chat.id, response.data.video_url,{caption:"Momenteel kun je een video niet reeten"})
          } else if(response.data.type == "Error"){
            bot.sendMessage(msg.chat.id, response.data.message)
          } else{
            bot.sendMessage(msg.chat.id, "Fatal error!")
          }
      })
      .catch((err) => {
        console.log(err);
      })
  },
  callback: function (bot, msg, image_id, reeten, type) {
    axios.get(config.BASEURL+':'+config.PORT+'/api/ImageRating/count?where={"user_id":"'+msg.from.id+'","chat_id":"'+msg.message.chat.id+'","image_id":'+image_id+',"image_type":"'+type+'"}')
    .then((response) => {
      if(response.data.count == 1){
        message = msg.from.first_name + " je hebt al gereet";
        bot.sendMessage(msg.message.chat.id, message.toString());
      } else{
      let firstname = "";
      let lastname = "";
      let username = "";
      try{firstname = msg.from.first_name} catch(err){firstname = "";}
      try{lastname = msg.from.last_name} catch(err){lastname = "";}
      try{username = msg.from.username} catch(err){username = "";}

        data = {
      "firstname": firstname,
      "lastname": lastname,
      "username": username,
          "chat_id": msg.message.chat.id,
          "user_id": msg.from.id,
          "image_id": image_id,
          "rating": reeten,
          "image_type": type
        };

        axios.post(config.BASEURL+':'+config.PORT+'/api/ImageRating', data)
        .then((response) => {
          axios.get(config.BASEURL+':'+config.PORT+'/api/ImageRating/count?where={"chat_id":"'+msg.message.chat.id+'","image_id":'+image_id+',"image_type":"'+type+'"}')
          .then((response) => {
            var count = response.data.count
            bot.getChatMembersCount(msg.message.chat.id)
            .then(response => {
              var memberCount = response
              console.log(memberCount)
              axios.get(config.BASEURL+':'+config.PORT+'/api/ImageRating/avg/'+type+'/'+image_id+'/'+msg.message.chat.id)
              .then(response => {
                var rating = response.data.avg;
                if(count == memberCount - 1){
                  bot.editMessageCaption("Reet hieronder: Gemiddelde rating("+Number.parseFloat(rating).toFixed(1)+")",{chat_id:msg.message.chat.id, message_id:msg.message.message_id});
                  bot.answerCallbackQuery(msg.id, "Je heb "+reeten+"\uD83C\uDF51 reeten gereet")
                } else{
                  var keyboardStr = JSON.stringify({
                    inline_keyboard: [
                      [
                      {text:'1\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":1, "image":'+image_id+'}'},
                      {text:'2\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":2, "image":'+image_id+'}'},
                      {text:'3\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":3, "image":'+image_id+'}'},
                      {text:'4\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":4, "image":'+image_id+'}'},
                      {text:'5\uD83C\uDF51',callback_data:'{"t":"'+type+'","reeten":5, "image":'+image_id+'}'}
                      ]
                    ]
                  });
                  var keyboard = JSON.parse(keyboardStr);
                  bot.editMessageCaption("Reet hieronder: Gemiddelde rating("+Number.parseFloat(rating).toFixed(1)+")",{reply_markup: keyboard,chat_id:msg.message.chat.id, message_id:msg.message.message_id});
                }
              })
              .catch(err => {
                console.log(err);
              });
            })
            .catch(err => {
              console.log(err);
            });
          })
          .catch((err) => {
            console.log(err);
          })
        })
        .catch((err) => {
          console.log(err);
        })
      }
    })
    .catch((err) => {
      console.log(err);
    })
  }
}