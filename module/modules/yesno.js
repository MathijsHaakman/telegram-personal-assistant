const axios = require('axios');

module.exports = {
  action: function (bot, msg, match) {
    axios.get('https://yesno.wtf/api')
		.then(response => {
			bot.sendVideo(msg.chat.id, response.data.image)
		})
		.catch(err => {
			console.log(err);
		});
  }
}