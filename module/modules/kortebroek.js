const axios = require('axios');
const cheerio = require("cheerio");

module.exports = {
  action: function (bot, msg, match) {
    axios.get('http://www.kanikeenkortebroekaan.nl/')
  	.then((response) => {

  	  var $ = cheerio.load(response.data);
  		var image = $(".main-image");
  		image = image.html();
  		image = image.split("\"");
  		image = image[1].split(".");
  		image = "http://www.kanikeenkortebroekaan.nl"+image[0]+".png";
      bot.sendPhoto(msg.chat.id, image)
	  })
  	.catch((err) => {
      console.log(err);
	  })
  }
}